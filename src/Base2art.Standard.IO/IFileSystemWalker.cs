namespace Base2art.IO
{
    using System.Collections.Generic;
    using System.IO;

    public interface IFileSystemSearcher
    {
        IReadOnlyList<IFileMatch<FileSystemInfo>> Walk(DirectoryInfo startingPoint);

        IReadOnlyList<IFileMatch<DirectoryInfo>> WalkDirectories(DirectoryInfo startingPoint);

        IReadOnlyList<IFileMatch<FileInfo>> WalkFiles(DirectoryInfo startingPoint);
    }
}