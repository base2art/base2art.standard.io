﻿namespace Base2art.IO
{
    public enum MatchingMode
    {
        Contained,
        AllowExitingRoot
    }
}