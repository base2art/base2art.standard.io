namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class FileSystemWalker : IFileSystemSearcher
    {
        private readonly Func<IFileMatch<DirectoryInfo>, bool> descendIntoDirectory;
        private readonly Func<IFileMatch<DirectoryInfo>, bool> includeDirectory;
        private readonly Func<IFileMatch<FileInfo>, bool> includeFile;

        public FileSystemWalker(
            Func<IFileMatch<DirectoryInfo>, bool> descendIntoDirectory,
            Func<IFileMatch<DirectoryInfo>, bool> includeDirectory,
            Func<IFileMatch<FileInfo>, bool> includeFile)
        {
            this.descendIntoDirectory = descendIntoDirectory;
            this.includeDirectory = includeDirectory;
            this.includeFile = includeFile;
        }

        public IReadOnlyList<IFileMatch<FileSystemInfo>> Walk(DirectoryInfo startingPoint)
        {
            if (!startingPoint.Exists)
            {
                throw new DirectoryNotFoundException(startingPoint.FullName);
            }

            var directoryInfos = new List<IFileMatch<DirectoryInfo>>();
            var fileInfos = new List<IFileMatch<FileInfo>>();

            this.WalkDirectory(startingPoint, startingPoint, new string[0], directoryInfos, fileInfos);

            return directoryInfos.Select<IFileMatch<DirectoryInfo>, IFileMatch<FileSystemInfo>>(x => x)
                                 .Concat(fileInfos.Select<IFileMatch<FileInfo>, IFileMatch<FileSystemInfo>>(x => x))
                                 .ToList();
        }

        public IReadOnlyList<IFileMatch<DirectoryInfo>> WalkDirectories(DirectoryInfo startingPoint)
        {
            if (!startingPoint.Exists)
            {
                throw new DirectoryNotFoundException(startingPoint.FullName);
            }

            var directoryInfos = new List<IFileMatch<DirectoryInfo>>();

            this.WalkDirectory(startingPoint, startingPoint, new string[0], directoryInfos, null);

            return directoryInfos;
        }

        public IReadOnlyList<IFileMatch<FileInfo>> WalkFiles(DirectoryInfo startingPoint)
        {
            if (!startingPoint.Exists)
            {
                throw new DirectoryNotFoundException(startingPoint.FullName);
            }

            var fileInfos = new List<IFileMatch<FileInfo>>();

            this.WalkDirectory(startingPoint, startingPoint, new string[0], null, fileInfos);

            return fileInfos;
        }

        private void WalkDirectory(
            DirectoryInfo startingPoint,
            DirectoryInfo currentDirectory,
            string[] parts,
            List<IFileMatch<DirectoryInfo>> directoryInfos,
            List<IFileMatch<FileInfo>> fileInfos)
        {
            foreach (var directory in currentDirectory.GetDirectories())
            {
                var newParts = parts.Concat(new[] {directory.Name}).ToArray();
                var fm = new FileMatch<DirectoryInfo>
                         {
                             RelativeName = Path.Combine(newParts),
                             RelativeParts = newParts,
                             FileEntry = directory
                         };

                if (directoryInfos != null)
                {
                    if (this.includeDirectory(fm))
                    {
                        directoryInfos.Add(fm);
                    }
                }

                if (this.descendIntoDirectory(fm))
                {
                    this.WalkDirectory(startingPoint, directory, newParts, directoryInfos, fileInfos);
                }
            }

            if (fileInfos != null)
            {
                foreach (var fileInfo in currentDirectory.GetFiles())
                {
                    var newParts = parts.Concat(new[] {fileInfo.Name}).ToArray();
                    var fm = new FileMatch<FileInfo>
                             {
                                 RelativeName = Path.Combine(newParts),
                                 RelativeParts = newParts,
                                 FileEntry = fileInfo
                             };

                    if (this.includeFile(fm))
                    {
                        fileInfos.Add(fm);
                    }
                }
            }
        }
    }
}