namespace Base2art.IO
{
    using System.IO;

    public static class Files
    {
        private static readonly FileSystemEntryRemover<IOException> FileItems = new FileSystemEntryRemover<IOException>(File.Exists, File.Delete);

        public static void DeleteOnExit(string pathFile)
        {
            FileItems.DeleteOnExit(pathFile);
        }
    }
}