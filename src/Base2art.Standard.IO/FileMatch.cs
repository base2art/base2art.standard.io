﻿namespace Base2art.IO
{
    using System.Collections.Generic;
    using System.IO;

    internal class FileMatch<T> : IFileMatch<T>
        where T : FileSystemInfo
    {
        public string RelativeName { get; set; }
        public IReadOnlyList<string> RelativeParts { get; set; }
        public T FileEntry { get; set; }
    }
}