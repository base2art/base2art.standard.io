﻿namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    public static class WildCardSearcher
    {
        public static bool IsFileIgnored(string relativePath, IEnumerable<string> ignores)
        {
            foreach (var wildCard in ignores)
            {
                var result = IsFileMatch(relativePath, wildCard);
                if (result.HasValue && result.Value)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     PATTERN FORMAT
        ///     A blank line matches no files, so it can serve as a separator for readability.
        ///     A line starting with # serves as a comment.
        ///     An optional prefix ! which negates the pattern; any matching file excluded by a previous pattern will become
        ///     included again. If a negated pattern matches, this will override lower precedence patterns sources.
        ///     If the pattern ends with a slash, it is removed for the purpose of the following description, but it would only
        ///     find a match with a directory. In other words, foo/ will match a directory foo and paths underneath it, but will
        ///     not match a regular file or a symbolic link foo (this is consistent with the way how pathspec works in general in
        ///     git).
        ///     If the pattern does not contain a slash /, git treats it as a shell glob pattern and checks for a match against the
        ///     pathname relative to the location of the .gitignore file (relative to the toplevel of the work tree if not from a
        ///     .gitignore file).
        ///     Otherwise, git treats the pattern as a shell glob suitable for consumption by fnmatch(3) with the FNM_PATHNAME
        ///     flag: wildcards in the pattern will not match a / in the pathname. For example, Documentation/*.html matches
        ///     Documentation/git.html but not Documentation/ppc/ppc.html or tools/perf/Documentation/perf.html.
        ///     A leading slash matches the beginning of the pathname. For example, /*.c matches cat-file.c but not
        ///     mozilla-sha1/sha1.c.
        /// </summary>
        /// <returns></returns>
        public static bool? IsFileMatch(string relativePath, string pattern)
        {
            if (string.IsNullOrWhiteSpace(pattern))
            {
                return null;
            }

            if (string.IsNullOrEmpty(relativePath))
            {
                return null;
            }

            pattern = pattern.Trim();

            if (pattern.StartsWith("#"))
            {
                return null;
            }

            Func<bool, bool> negator = x => x;
            if (pattern.StartsWith("!"))
            {
                negator = x => !x;
                pattern = pattern.Substring(1);
            }

            var isDir = false;
            if (pattern.Contains("/"))
            {
                isDir = true;
            }

            return negator(IsFileMatchInternal(relativePath, pattern, isDir));
        }

        public static bool IsWildCardMatch(string value, string pattern) => IsWildCardMatchInternal(value, pattern, true, true);

        public static bool IsWildCardPrefixMatch(string value, string pattern) => IsWildCardMatchInternal(value, pattern, true, false);

        public static bool IsWildCardSuffixMatch(string value, string pattern) => IsWildCardMatchInternal(value, pattern, false, true);

        public static bool IsWildCardFound(string value, string pattern) => IsWildCardMatchInternal(value, pattern, false, false);

        private static bool IsFileMatchInternal(string relativePath, string pattern, bool isDir)
        {
            if (!isDir)
            {
                var parts = relativePath.Split(new[] {'\\', '/'}, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Any(x => IsWildCardMatch(x, pattern)))
                {
                    return true;
                }
            }

            return IsDirMatchInternal(
                                      relativePath.Split(new[] {'\\', '/'}, StringSplitOptions.RemoveEmptyEntries),
                                      pattern.Split(new[] {'\\', '/'}, StringSplitOptions.RemoveEmptyEntries),
                                      pattern.EndsWith("\\") || pattern.EndsWith("/")).GetValueOrDefault();
        }

        private static bool? IsDirMatchInternal(string[] relativePathParts, string[] patternParts, bool endsWith)
        {
            if (relativePathParts.Length == 0)
            {
                return null;
            }

            if (patternParts.Length == 0)
            {
                return null;
            }

            if (endsWith && patternParts.Length == 1)
            {
                return relativePathParts.Reverse().Skip(1).Reverse().Any(x => IsWildCardMatch(x, patternParts[0]));
            }

            if (patternParts.Length > relativePathParts.Length)
            {
                return false;
            }

            var isMatch = true;

            for (var i = 0; i < patternParts.Length; i++)
            {
                var patternPart = patternParts[i];
                var relativePart = relativePathParts[i];
                if (!IsWildCardMatch(relativePart, patternPart))
                {
                    isMatch = false;
                }
            }

            return isMatch;
        }

        private static bool IsWildCardMatchInternal(string value, string pattern, bool pegStart, bool pegEnd)
        {
            var options = RegexOptions.Singleline |
                          RegexOptions.CultureInvariant |
                          RegexOptions.IgnoreCase;

            var regexString = ConvertLikeExpression(pattern, pegStart, pegEnd);
            var regexpr = new Regex(regexString, options);

            return regexpr.IsMatch(value);
        }

        private static string ConvertLikeExpression(string expression, bool pegStart, bool pegEnd)
        {
            var sb = new StringBuilder();

            if (pegStart)
            {
                sb.Append("^");
            }

            for (var pos = 0; pos <= expression.Length - 1; pos++)
            {
                switch (expression[pos])
                {
                    case '?':
                        sb.Append('.');
                        break;
                    case '*':
                        sb.Append('.').Append('*');
                        break;
                    case '#':
                        sb.Append("\\d{1}");
                        break;
                    case '[':
                        var gsb = ConvertGroupSubexpression(expression, ref pos);

                        // skip groups of form [], i.e. empty strings
                        if (gsb.Length > 2)
                        {
                            sb.Append(gsb);
                        }

                        break;
                    default:
                        sb.Append(Regex.Escape(expression[pos].ToString()));
                        break;
                }
            }

            if (pegEnd)
            {
                sb.Append("$");
            }

            return sb.ToString();
        }

        private static StringBuilder ConvertGroupSubexpression(string carr, ref int pos)
        {
            var sb = new StringBuilder();
            var negate = false;

            while (carr[pos] != ']')
            {
                if (negate)
                {
                    sb.Append('^');
                    negate = false;
                }

                if (carr[pos] == '!')
                {
                    sb.Remove(1, sb.Length - 1);
                    negate = true;
                }
                else
                {
                    sb.Append(carr[pos]);
                }

                pos = pos + 1;
            }

            sb.Append(']');

            return sb;
        }
    }
}