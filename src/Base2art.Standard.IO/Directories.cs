namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Loader;

    public static class Directories
    {
        private static readonly FileSystemEntryRemover<IOException> Files
            = new FileSystemEntryRemover<IOException>(Directory.Exists, x => Directory.Delete(x, true));

        public static void DeleteOnExit(string pathFile)
        {
            Files.DeleteOnExit(pathFile);
        }
    }
}