﻿namespace Base2art.IO
{
    using System.Collections.Generic;
    using System.IO;

    public interface IFileMatch<out T>
        where T : FileSystemInfo
    {
        string RelativeName { get; }

        IReadOnlyList<string> RelativeParts { get; }

        T FileEntry { get; }
    }
}