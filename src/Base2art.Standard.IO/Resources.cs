﻿namespace Base2art.IO
{
    using System.IO;
    using System.Reflection;

    public static class Resources
    {
        public static byte[] GetResourceAsBytes(this Assembly asm, string resourceName)
        {
            using (var asmStream = GetAsmStream(asm, resourceName))
            {
                return asmStream.ReadFully();
            }
        }

        public static string GetResourceAsText(this Assembly asm, string resourceName)
        {
            using (var asmStream = GetAsmStream(asm, resourceName))
            {
                using (var sr = new StreamReader(asmStream))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        private static Stream GetAsmStream(Assembly asm, string resourceName)
        {
            resourceName = resourceName.Replace('/', '.');
            var asmStream = asm.GetManifestResourceStream(resourceName);

            if (asmStream != null)
            {
                return asmStream;
            }

            return asm.GetManifestResourceStream(asm.GetName().Name + "." + resourceName);
        }
    }
}