namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Loader;

    internal class FileSystemEntryRemover<T> where T : Exception
    {
        private readonly Func<string, bool> exists;
        private readonly Action<string> remove;

        private readonly Stack<string> filesToRemove = new Stack<string>();

        public FileSystemEntryRemover(Func<string, bool> exists, Action<string> remove)
        {
            this.exists = exists;
            this.remove = remove;
        }

        public void DeleteOnExit(string path)
        {
            if (this.filesToRemove.Count == 0)
            {
                this.AttachEvent();
            }

            this.filesToRemove.Push(path);
        }

        private void AttachEvent()
        {
            AssemblyLoadContext.Default.Unloading += this.HandleUnload;
        }

        private void HandleUnload(AssemblyLoadContext obj)
        {
            var strings = this.filesToRemove.Select(x => x).ToArray();
            foreach (var file in strings)
            {
                this.TryDelete(file, 0, 3);
            }
        }

        private void TryDelete(string file, int current, int max)
        {
            if (!this.exists(file))
            {
                return;
            }

            try
            {
                this.remove(file);
            }
            catch (T)
            {
                if (current > max)
                {
                    return;
                }

                this.TryDelete(file, current + 1, max);
            }
        }
    }
}