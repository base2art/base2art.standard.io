﻿namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class FileEntrySearcher
    {
        public static IFileMatch<FileInfo>[] FindAllFiles(this DirectoryInfo directory)
        {
            var walker = new FileSystemWalker(
                                              x => true,
                                              x => false,
                                              x => true);

            return walker.WalkFiles(directory).ToArray();
        }

        public static IFileMatch<DirectoryInfo>[] FindAllDirectories(this DirectoryInfo directory)
        {
            var walker = new FileSystemWalker(
                                              x => true,
                                              x => true,
                                              x => false);
            return walker.WalkDirectories(directory).ToArray();
        }

        public static IFileMatch<FileInfo>[] FindMatchingFiles(this DirectoryInfo directory, string[] wildCards, MatchingMode matchingMode)
        {
            var wildCardFilters = CleanWildCards(wildCards);

            IFileSystemSearcher Walker()
            {
                switch (matchingMode)
                {
                    case MatchingMode.Contained:
                        return new FileSystemWalker(
                                                    x => Descend(x, wildCardFilters, false),
                                                    x => false,
                                                    x => Descend(x, wildCardFilters, true));
                    case MatchingMode.AllowExitingRoot:
                        return new FileSystemSearcher(wildCardFilters);
                    default:
                        throw new ArgumentOutOfRangeException(nameof(matchingMode), "Mode Not supported");
                }
            }

            return Walker().WalkFiles(directory).ToArray();
        }

        public static IFileMatch<DirectoryInfo>[] FindMatchingDirectories(this DirectoryInfo directory, string[] wildCards, MatchingMode matchingMode)
        {
            var wildCardFilters = CleanWildCards(wildCards);

            IFileSystemSearcher Walker()
            {
                switch (matchingMode)
                {
                    case MatchingMode.Contained:
                        return new FileSystemWalker(
                                                    x => Descend(x, wildCardFilters, false),
                                                    x => Descend(x, wildCardFilters, true),
                                                    x => false);
                    case MatchingMode.AllowExitingRoot:
                        return new FileSystemSearcher(wildCardFilters);
                    default:
                        throw new ArgumentOutOfRangeException(nameof(matchingMode), "Mode Not supported");
                }
            }

            return Walker().WalkDirectories(directory).ToArray();
        }

        private static string[] GetWildCardParts(string wildCard)
        {
            var items = new LinkedList<string>();
            while (!string.IsNullOrWhiteSpace(wildCard))
            {
                var fileName = Path.GetFileName(wildCard);
                var directoryName = Path.GetDirectoryName(wildCard);

                items.AddFirst(fileName);
                wildCard = directoryName;
            }

            return items.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        }

        private static string[][] CleanWildCards(string[] wildCards)
        {
            return (wildCards ?? new string[0])
                   .Where(x => !string.IsNullOrWhiteSpace(x))
                   .Select(x => x.Trim())
                   .Where(x => !x.StartsWith("#", StringComparison.Ordinal))
                   .Select(GetWildCardParts)
                   .ToArray();
        }

        private static bool Descend<T>(IFileMatch<T> arg, string[][] wildCardFilters, bool requireEnd)
            where T : FileSystemInfo
        {
            var parts = arg.RelativeParts;

            foreach (var wildCardParts in wildCardFilters)
            {
                var currentPart = parts[parts.Count - 1];

                if (wildCardParts.Length >= parts.Count)
                {
                    var currentWildCard = wildCardParts[parts.Count - 1];

                    if (WildCardSearcher.IsWildCardMatch(currentPart, currentWildCard))
                    {
                        if (requireEnd)
                        {
                            if (wildCardParts.Length == parts.Count)
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}