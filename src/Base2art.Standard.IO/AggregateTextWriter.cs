﻿namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AggregateTextWriter : TextWriter
    {
        private readonly TextWriter[] backings;

        public AggregateTextWriter(params TextWriter[] backings) => this.backings = backings ?? new TextWriter[0];

        public override Encoding Encoding => this.backings.FirstOrDefault()?.Encoding ?? Encoding.UTF8;

        public override void Write(char value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Flush()
        {
            this.ForEach(x => x.Flush());
        }

        public override Task FlushAsync()
        {
            return Task.WhenAll(this.ForEach(x => x.FlushAsync()));
        }

        public override void Write(bool value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(char[] buffer)
        {
            this.ForEach(x => x.Write(buffer));
        }

        public override void Write(char[] buffer, int index, int count)
        {
            this.ForEach(x => x.Write(buffer, index, count));
        }

        public override void Write(decimal value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(double value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(int value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(long value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(object value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(float value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(string value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(string format, object arg0)
        {
            this.ForEach(x => x.Write(format, arg0));
        }

        public override void Write(string format, object arg0, object arg1)
        {
            this.ForEach(x => x.Write(format, arg0, arg1));
        }

        public override void Write(string format, object arg0, object arg1, object arg2)
        {
            this.ForEach(x => x.Write(format, arg0, arg1, arg2));
        }

        public override void Write(string format, params object[] arg)
        {
            this.ForEach(x => x.Write(format, arg));
        }

        public override void Write(uint value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override void Write(ulong value)
        {
            this.ForEach(x => x.Write(value));
        }

        public override Task WriteAsync(char value)
        {
            return Task.WhenAll(this.ForEach(x => x.WriteAsync(value)));
        }

        public override Task WriteAsync(char[] buffer, int index, int count)
        {
            return Task.WhenAll(this.ForEach(x => x.WriteAsync(buffer, index, count)));
        }

        public override Task WriteAsync(string value)
        {
            return Task.WhenAll(this.ForEach(x => x.WriteAsync(value)));
        }

        public override void WriteLine()
        {
            this.ForEach(x => x.WriteLine());
        }

        public override void WriteLine(bool value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(char value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(char[] buffer)
        {
            this.ForEach(x => x.WriteLine(buffer));
        }

        public override void WriteLine(char[] buffer, int index, int count)
        {
            this.ForEach(x => x.WriteLine(buffer, index, count));
        }

        public override void WriteLine(decimal value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(double value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(int value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(long value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(object value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(float value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(string value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(string format, object arg0)
        {
            this.ForEach(x => x.WriteLine(format, arg0));
        }

        public override void WriteLine(string format, object arg0, object arg1)
        {
            this.ForEach(x => x.WriteLine(format, arg0, arg1));
        }

        public override void WriteLine(string format, object arg0, object arg1, object arg2)
        {
            this.ForEach(x => x.WriteLine(format, arg0, arg1, arg2));
        }

        public override void WriteLine(string format, params object[] arg)
        {
            this.ForEach(x => x.WriteLine(format, arg));
        }

        public override void WriteLine(uint value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override void WriteLine(ulong value)
        {
            this.ForEach(x => x.WriteLine(value));
        }

        public override Task WriteLineAsync()
        {
            return Task.WhenAll(this.ForEach(x => x.WriteLineAsync()));
        }

        public override Task WriteLineAsync(char value)
        {
            return Task.WhenAll(this.ForEach(x => x.WriteLineAsync(value)));
        }

        public override Task WriteLineAsync(char[] buffer, int index, int count)
        {
            return Task.WhenAll(this.ForEach(x => x.WriteLineAsync(buffer, index, count)));
        }

        public override Task WriteLineAsync(string value)
        {
            return Task.WhenAll(this.ForEach(x => x.WriteLineAsync(value)));
        }

        private void ForEach(Action<TextWriter> func)
        {
            foreach (var backing in this.backings)
            {
                func(backing);
            }
        }

        private IEnumerable<T> ForEach<T>(Func<TextWriter, T> func)
            => this.backings.Select(func);
    }
}