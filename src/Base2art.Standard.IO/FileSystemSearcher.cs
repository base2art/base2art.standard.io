namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class FileSystemSearcher : IFileSystemSearcher
    {
        private readonly string[][] wildCardFilters;

        public FileSystemSearcher(string[][] wildCardFilters) => this.wildCardFilters = wildCardFilters;

        public IReadOnlyList<IFileMatch<FileSystemInfo>> Walk(DirectoryInfo startingPoint)
        {
            if (!startingPoint.Exists)
            {
                throw new DirectoryNotFoundException(startingPoint.FullName);
            }

            var directoryInfos = new List<IFileMatch<DirectoryInfo>>();
            var fileInfos = new List<IFileMatch<FileInfo>>();

            this.Walker(startingPoint, startingPoint, new string[0], this.wildCardFilters, 0, directoryInfos, fileInfos);

            return directoryInfos.Select<IFileMatch<DirectoryInfo>, IFileMatch<FileSystemInfo>>(x => x)
                                 .Concat(fileInfos.Select<IFileMatch<FileInfo>, IFileMatch<FileSystemInfo>>(x => x))
                                 .ToList();
        }

        public IReadOnlyList<IFileMatch<DirectoryInfo>> WalkDirectories(DirectoryInfo startingPoint)
        {
            if (!startingPoint.Exists)
            {
                throw new DirectoryNotFoundException(startingPoint.FullName);
            }

            var directoryInfos = new List<IFileMatch<DirectoryInfo>>();

            this.Walker(startingPoint, startingPoint, new string[0], this.wildCardFilters, 0, directoryInfos, null);

            return directoryInfos;
        }

        public IReadOnlyList<IFileMatch<FileInfo>> WalkFiles(DirectoryInfo startingPoint)
        {
            if (!startingPoint.Exists)
            {
                throw new DirectoryNotFoundException(startingPoint.FullName);
            }

            var fileInfos = new List<IFileMatch<FileInfo>>();

            this.Walker(startingPoint, startingPoint, new string[0], this.wildCardFilters, 0, null, fileInfos);

            return fileInfos;
        }

        private void Walker(
            DirectoryInfo startingPoint,
            DirectoryInfo currentPoint,
            string[] relativeParts,
            string[][] cardFilters,
            int i,
            List<IFileMatch<DirectoryInfo>> directoryInfos,
            List<IFileMatch<FileInfo>> fileInfos)
        {
            var parts = cardFilters
                        .Where(x => i < x.Length)
                        .GroupBy(x => x[i], StringComparer.OrdinalIgnoreCase)
                        .Select(x => Tuple.Create(x.Key, x.Select(y => y.Length).Distinct().ToArray()));

            foreach (var partPair in parts)
            {
                var counts = partPair.Item2;
                var part = partPair.Item1;

                var directoryChildren = this.Explode(
                                                     currentPoint,
                                                     part,
                                                     info => new DirectoryInfo(info),
                                                     x => x.GetDirectories());

                foreach (var dir in directoryChildren)
                {
                    var newParts = relativeParts.Concat(new[] {dir.Name}).ToArray();
                    this.Walker(startingPoint, dir, newParts, cardFilters, i + 1, directoryInfos, fileInfos);

                    if (directoryInfos != null && counts.Contains(i + 1) && dir.Exists)
                    {
                        directoryInfos.Add(new FileMatch<DirectoryInfo>
                                           {
                                               RelativeParts = newParts,
                                               RelativeName = Path.Combine(newParts),
                                               FileEntry = dir
                                           });
                    }
                }

                var fileChildren = this.Explode(
                                                currentPoint,
                                                part,
                                                info => new FileInfo(info),
                                                x => x.GetFiles());

                foreach (var file in fileChildren)
                {
                    var newParts = relativeParts.Concat(new[] {file.Name}).ToArray();

                    if (fileInfos != null && counts.Contains(i + 1) && file.Exists)
                    {
                        fileInfos.Add(new FileMatch<FileInfo>
                                      {
                                          RelativeParts = newParts,
                                          RelativeName = Path.Combine(newParts),
                                          FileEntry = file
                                      });
                    }
                }
            }
        }

        private T[] Explode<T>(
            DirectoryInfo currentPoint,
            string part,
            Func<string, T> createChild,
            Func<DirectoryInfo, T[]> getChildren)
            where T : FileSystemInfo
        {
            if (part.Contains('?') || part.Contains('*'))
            {
                var items = new List<T>();
                if (currentPoint.Exists)
                {
                    foreach (var dir in getChildren(currentPoint))
                    {
                        if (WildCardSearcher.IsWildCardFound(dir.Name, part))
                        {
                            items.Add(dir);
                        }
                    }
                }

                return items.ToArray();
            }

            return new[]
                   {
                       createChild(Path.Combine(currentPoint.FullName, part))
                   };
        }
    }
}