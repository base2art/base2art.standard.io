﻿namespace Base2art.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class FileSystems
    {
        public static DirectoryInfo Parent(this DirectoryInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            return info.Parent;
        }

        public static DirectoryInfo Dir(this DirectoryInfo info, string name)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            return info.EnumerateDirectories()
                       .FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        public static FileInfo File(this DirectoryInfo info, string name)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            return info.EnumerateFiles()
                       .FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        public static string[] FindFilteredDirectories(string basePath, string relativePath)
            => FindFilteredDirectories(basePath, relativePath, new string[0]);

        private static string[] FindFilteredDirectories(string basePath, string relativePath, string[] parts)
        {
            var directoryPath = Path.GetDirectoryName(relativePath);
            var currentPath = Path.GetFileName(relativePath);

            if (!string.IsNullOrWhiteSpace(currentPath))
            {
                parts = new[] {currentPath}.Concat(parts).ToArray();
            }

            return directoryPath?.Contains("*") != true
                       ? Build(new[] {Path.Combine(basePath, directoryPath)}, parts)
                       : FindFilteredDirectories(basePath, directoryPath, parts);
        }

        private static string[] Build(string[] basePaths, string[] parts)
        {
            if (parts.Length == 0)
            {
                return basePaths;
            }

            var nextPart = parts[0];

            if (nextPart != "*")
            {
                return Build(
                             basePaths.Select(x => Path.Combine(x, nextPart)).Where(Directory.Exists).ToArray(),
                             parts.Skip(1).ToArray());
            }

            var newBases = new List<string>();

            foreach (var basePath in basePaths)
            {
                var dirs = Directory.GetDirectories(basePath).Select(x => Path.GetFileName(x));
                foreach (var dir in dirs)
                {
                    newBases.Add(Path.Combine(basePath, dir));
                }
            }

            return Build(newBases.ToArray(), parts.Skip(1).ToArray());
        }
    }
}