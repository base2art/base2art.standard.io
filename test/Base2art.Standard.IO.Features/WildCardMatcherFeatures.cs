namespace Base2art.Standard.IO.Features
{
    using Base2art.IO;
    using FluentAssertions;
    using Xunit;

    public class WildCardMatcherFeatures
    {
        [Theory]
        [InlineData("-1d", "-#*", "1", true)]
        [InlineData("1", "#", "2", true)]
        [InlineData("12", "#", "3", false)]
        [InlineData("aa", "?", "4", false)]
        [InlineData("F", "F", "5", true)]
        [InlineData("F", "F", "6", true)]
        // No longer a valid test case
        // [InlineData("F", "f", "7", false)]
        [InlineData("F", "f", "8", true)]
        [InlineData("F", "FFF", "9", false)]
        [InlineData("aBBBa", "a*a", "10", true)]
        [InlineData("F", "[A-Z]", "11", true)]
        [InlineData("F", "[!A-Z]", "12", false)]
        [InlineData("a2a", "a#a", "13", true)]
        [InlineData("aM5b", "a[L-P]#[!c-e]", "14", true)]
        [InlineData("BAT123khg", "B?T*", "15", true)]
        [InlineData("CAT123khg", "B?T*", "16", false)]
        public void ShouldMatch(string source, string pattern, string ignore, bool result)
        {
            WildCardSearcher.IsWildCardMatch(source, pattern).Should().Be(result, ignore);
        }

        [Theory]
        [InlineData("hello/debug/", "[Dd]ebug/", "", true)]
        [InlineData("-1d", "-#*", "1", true)]
        [InlineData("1", "#", "2", true)]
        [InlineData("12", "#", "3", true)]
        [InlineData("aa", "?", "4", true)]
        [InlineData("F", "F", "5", true)]
        [InlineData("F", "F", "6", true)]
        // No longer a valid test case
        // [InlineData("F", "f", "7", false)]
        [InlineData("F", "f", "8", true)]
        [InlineData("F", "FFF", "9", false)]
        [InlineData("aBBBa", "a*a", "10", true)]
        [InlineData("F", "[A-Z]", "11", true)]
        [InlineData("F", "[!A-Z]", "12", false)]
        [InlineData("a2a", "a#a", "13", true)]
        [InlineData("aM5b", "a[L-P]#[!c-e]", "14", true)]
        [InlineData("BAT123khg", "B?T*", "15", true)]
        [InlineData("CAT123khg", "B?T*", "16", false)]
        public void ShouldFind(string source, string pattern, string ignore, bool result)
        {
            WildCardSearcher.IsWildCardFound(source, pattern).Should().Be(result, ignore);
        }

        /// <summary>
        ///     PATTERN FORMAT
        ///     A blank line matches no files, so it can serve as a separator for readability.
        ///     A line starting with # serves as a comment.
        ///     An optional prefix ! which negates the pattern; any matching file excluded by a previous pattern will become
        ///     included again. If a negated pattern matches, this will override lower precedence patterns sources.
        ///     If the pattern ends with a slash, it is removed for the purpose of the following description, but it would only
        ///     find a match with a directory. In other words, foo/ will match a directory foo and paths underneath it, but will
        ///     not match a regular file or a symbolic link foo (this is consistent with the way how pathspec works in general in
        ///     git).
        ///     If the pattern does not contain a slash /, git treats it as a shell glob pattern and checks for a match against the
        ///     pathname relative to the location of the .gitignore file (relative to the toplevel of the work tree if not from a
        ///     .gitignore file).
        ///     Otherwise, git treats the pattern as a shell glob suitable for consumption by fnmatch(3) with the FNM_PATHNAME
        ///     flag: wildcards in the pattern will not match a / in the pathname. For example, Documentation/*.html matches
        ///     Documentation/git.html but not Documentation/ppc/ppc.html or tools/perf/Documentation/perf.html.
        ///     A leading slash matches the beginning of the pathname. For example, /*.c matches cat-file.c but not
        ///     mozilla-sha1/sha1.c.
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData("", "", null)]
        [InlineData("test.cs", "", null)]
        [InlineData("", "test.cs", null)]
        [InlineData("  ", "test.cs", false)]
        [InlineData("1 test.cs", "# test.cs", null)]
        [InlineData("test.cs", "# test.cs", null)]
        [InlineData("test.cs", "test.cs", true)]
        [InlineData("test.cs", "!test.cs", false)]
        [InlineData("tst.cs", "!test.cs", true)]
        [InlineData("!test.cs", "!test.cs", true)]
        [InlineData("test.cs", "test.cs/", false)]
        [InlineData("test.cs/index.aspx", "test.cs/", true)]
        [InlineData("other/test.cs/index.aspx", "test.cs/", true)]
        [InlineData("Documentation/git.html", "Documentation/*.html", true)]
        [InlineData("Documentation/ppc/ppc.html", "Documentation/*.html", false)]
        [InlineData("tools/perf/Documentation/perf.html", "Documentation/*.html", false)]
        [InlineData("tools/perf/Documentation/perf.html", "Documentation/*.html/", false)]
        [InlineData("tools/perf/Documentation/perf.html", "perf/Documentation/", false)]
        [InlineData("tools/perf/Documentation/perf.html", "perf/Documentation", false)]
        public void ShouldMatchDir(string fileName, string pattern, bool? expected)
        {
            WildCardSearcher.IsFileMatch(fileName, pattern)
                            .Should()
                            .Be(expected, "because '{0}' does not match '{1}'", pattern, fileName);
        }

        [Fact]
        public void ShouldLoad()
        {
            Assert.True(WildCardSearcher.IsWildCardMatch("-1d", "-#*"), "1");
            Assert.True(WildCardSearcher.IsWildCardMatch("1", "#"), "2");
            Assert.False(WildCardSearcher.IsWildCardMatch("12", "#"), "3");
            Assert.False(WildCardSearcher.IsWildCardMatch("aa", "?"), "4");
            Assert.True(WildCardSearcher.IsWildCardMatch("F", "F"), "5");
            Assert.True(WildCardSearcher.IsWildCardMatch("F", "F"), "6");
            Assert.True(WildCardSearcher.IsWildCardMatch("F", "f"), "8");
            Assert.False(WildCardSearcher.IsWildCardMatch("F", "FFF"), "9");
            Assert.True(WildCardSearcher.IsWildCardMatch("aBBBa", "a*a"), "10");
            Assert.True(WildCardSearcher.IsWildCardMatch("F", "[A-Z]"), "11");
            Assert.False(WildCardSearcher.IsWildCardMatch("F", "[!A-Z]"), "12");
            Assert.True(WildCardSearcher.IsWildCardMatch("a2a", "a#a"), "13");
            Assert.True(WildCardSearcher.IsWildCardMatch("aM5b", "a[L-P]#[!c-e]"), "14");
            Assert.True(WildCardSearcher.IsWildCardMatch("BAT123khg", "B?T*"), "15");
            Assert.False(WildCardSearcher.IsWildCardMatch("CAT123khg", "B?T*"), "16");
        }
    }
}