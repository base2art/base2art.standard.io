namespace Base2art.Standard.IO.Features
{
    using System;
    using System.IO;
    using System.Reflection;
    using Base2art.IO;
    using FluentAssertions;
    using Xunit;

    public class FileSystemExtenderFeature
    {
        [Fact]
        public void ShouldGetChildDir()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            di.Parent.Parent.Dir(di.Parent.Name).FullName.Should().BeEquivalentTo(di.Parent.FullName);
        }

        [Fact]
        public void ShouldGetChildFile()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            di.Parent.File(di.Name).FullName.Should().BeEquivalentTo(di.FullName);
        }

        [Fact]
        public void ShouldGetParent()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            di.Parent.FullName.Should().BeEquivalentTo(di.Parent().FullName);
        }

        [Fact]
        public void ShouldValidateNulls()
        {
            var di = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);

            Action mut = () => FileSystems.Dir(null, "abc");
            mut.ShouldThrow<ArgumentNullException>();

            mut = () => di.Dir(null);
            mut.ShouldThrow<ArgumentNullException>();
        }
    }
}