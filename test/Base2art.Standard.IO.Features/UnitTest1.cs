namespace Base2art.Standard.IO.Features
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Base2art.IO;
    using FluentAssertions;
    using Xunit;
    using Xunit.Abstractions;

    public class MatcherFeature1
    {
        public MatcherFeature1(ITestOutputHelper helper) => this.helper = helper;

        private readonly ITestOutputHelper helper;

        [Theory]
        [InlineData("src/*/bin/{config}/netstandard1.0/", 3, 4)]
        [InlineData("src/*/bin/{config}/netstandard1.0", 3, 4)]
        [InlineData("src/*/bin/{config}/*/", 34, 336)]
        [InlineData("src/*/bin/{config}/netstandard2.0/", 27, 200)]
        [InlineData("src/*/bin/{config}/netstandard1.6/", 0, 0)]
        [InlineData("src/*/bin/{config}/netstandard1.3/", 2, 2)]
        public void Test1(string filter, int folderCount, int dllCount)
        {
            var cd = FileWriter.Write();

            var config = true;
//            Assembly.GetExecutingAssembly()
//                                 .GetCustomAttributes(false)
//                                 .OfType<DebuggableAttribute>()
//                                 .Any(da => da.IsJITTrackingEnabled);
            filter = filter.Replace("{config}", config ? "Debug" : "Release");

            this.helper.WriteLine(filter);
            var directories = cd.FindMatchingDirectories(new[] {filter}, MatchingMode.Contained).Length.Should().Be(folderCount);

            this.helper.WriteLine(filter);

            var newFilter = filter.EndsWith("/")
                                ? filter + "*.dll"
                                : filter + "/*.dll";

            cd.FindMatchingFiles(new[] {newFilter}, MatchingMode.Contained).Length.Should().Be(dllCount);
        }

        [Theory]
        [InlineData("../Base2art.Standard.IO/bin/{config}/netstandard1.3/", 0, 1)]
        [InlineData("../Base2art.Standard.IO/bin/{config}/*/", 0, 1)]
        [InlineData("../Base2art.Standard.IO/bin/", 0, 1)]
        [InlineData("../Base2art.Standard.IO/*/", 0, 2)]
        public void RealWorldExample(string filter, int counter1, int counter2)
        {
            var config = true;
//            Assembly.GetExecutingAssembly()
//                                 .GetCustomAttributes(false)
//                                 .OfType<DebuggableAttribute>()
//                                 .Any(da => da.IsJITTrackingEnabled);

            filter = filter.Replace("{config}", config ? "Debug" : "Release");

            var cd = FileWriter.Write();

            cd = new DirectoryInfo(Path.Combine(cd.FullName, "src", "Base2art.Standard.IO"));

            cd.FindMatchingDirectories(new[] {filter}, MatchingMode.Contained)
              .Length.Should().Be(counter1);

            var directories = cd.FindMatchingDirectories(new[] {filter}, MatchingMode.AllowExitingRoot);
            directories.Length.Should().Be(counter2);
        }

        [Theory]
        [InlineData("../Base2art.Standard.IO/bin/{config}/netstandard1.3/*.dll", 0, 1)]
        [InlineData("../Base2art.Standard.IO/bin/{config}/*/*.dll", 0, 1)]
        [InlineData("../Base2art.Standard.IO/bin/*.dll", 0, 0)]
        [InlineData("../Base2art.Standard.IO/*/*.dll", 0, 0)]
        [InlineData("../*/bin/{config}/netstandard2.0/*.dll", 0, 200)]
        [InlineData("../Base2art.WebApiRunner.Server.Core/bin/{config}/netstandard2.0/*.dll", 0, 7)]
        [InlineData("../Base2art.WebApiRunner.Server.Core/bin/{config}/netstandard2.0/*.pdb", 0, 7)]
        [InlineData("../Base2art.WebApiRunner.Server.Core/bin/{config}/netstandard2.0/*.json", 0, 1)]
        public void RealWorldExampleFile(string filter, int counter1, int counter2)
        {
            var cd = FileWriter.Write();

            cd = new DirectoryInfo(Path.Combine(cd.FullName, "src", "Base2art.Standard.IO"));

            var config = true;
//            Assembly.GetExecutingAssembly()
//                                 .GetCustomAttributes(false)
//                                 .OfType<DebuggableAttribute>()
//                                 .Any(da => da.IsJITTrackingEnabled);

            filter = filter.Replace("{config}", config ? "Debug" : "Release");

            cd.FindMatchingFiles(new[] {filter}, MatchingMode.Contained)
              .Length.Should().Be(counter1);

            var directories = cd.FindMatchingFiles(new[] {filter}, MatchingMode.AllowExitingRoot);
            directories.Length.Should().Be(counter2);
        }

        private DirectoryInfo GetRoot()
        {
            var cd = new DirectoryInfo(Environment.CurrentDirectory);
            while (cd != null && !File.Exists(Path.Combine(cd.FullName, "base2art.standard.IO.sln")))
            {
                cd = cd.Parent;
            }

            return cd;
        }

        [Fact]
        public void Test2()
        {
            var cd = FileWriter.Write();

            var config = true;
//            Assembly.GetExecutingAssembly()
//                                 .GetCustomAttributes(false)
//                                 .OfType<DebuggableAttribute>()
//                                 .Any(da => da.IsJITTrackingEnabled);

            var releaseName = config ? "Debug" : "Release";
            var filter = $"src/*/bin/{releaseName}/*/";
            var count = 34;

            var items = cd.FindMatchingDirectories(new[] {filter}, MatchingMode.Contained);

            this.helper.WriteLine(items.Length.ToString());

            items.Length.Should().Be(count);

            items = items.OrderBy(x => x.RelativeName).ToArray();

            items[0].RelativeName.Should()
                    .Be($"src\\Base2art.Standard.Caching\\bin\\{releaseName}\\netstandard2.0");

            var path = new DirectoryInfo(Path.Combine(cd.FullName, "src\\Base2art.Standard.Caching\\bin"));
            path.FindAllDirectories().Length.Should().Be(2);
            path.FindAllFiles().Length.Should().Be(3);
        }
    }
}