namespace Base2art.Standard.IO.Features
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using Base2art.IO;

    public static class FileWriter
    {
        public static readonly Guid id = Guid.NewGuid();

        public static DirectoryInfo Write()
        {
            var url = "Base2art.Standard.IO.Features.base2art.standard.webapirunner.zip";
            var asm = typeof(FileWriter).Assembly
                                        .GetResourceAsBytes(url);

            var pathDir = Path.Combine(Path.GetTempPath(), id.ToString("N")); // + ".zip");
            var pathFile = $"{pathDir}.zip";

            if (!File.Exists(pathFile))
            {
                File.WriteAllBytes(pathFile, asm);
                Files.DeleteOnExit(pathFile);
                Directories.DeleteOnExit(pathDir);
                Directory.CreateDirectory(pathDir);
                ZipFile.ExtractToDirectory(pathFile, pathDir);
//                return pathDir;
            }

            return new DirectoryInfo(Path.Combine(pathDir, "base2art.standard.webapirunner"));
        }
    }
}