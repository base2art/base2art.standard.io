//namespace Base2art.Standard.IO.Features
//{
//    using Base2art.IO;
//    using Xunit;
//
//    public class MatcherFeature
//    {
//        [Theory]
//        [InlineData(@"Z:\b2a\base2art.standard.webapirunner", "src/*/bin/Debug/netstandard1.0/", 6)]
//        [InlineData(@"Z:\b2a\base2art.standard.webapirunner", "src/*/bin/Debug/netstandard1.0", 6)]
//        [InlineData(@"Z:\b2a\base2art.standard.webapirunner", "src/*/bin/Debug/*/", 38)]
//        [InlineData(@"Z:\b2a\base2art.standard.webapirunner", "src/*/bin/Debug/netstandard2.0/", 23)]
//        [InlineData(@"Z:\b2a\base2art.standard.webapirunner", "src/*/bin/Debug/netstandard1.6/", 0)]
//        [InlineData(@"Z:\b2a\base2art.standard.webapirunner", "src/*/bin/Debug/netstandard1.3/", 5)]
//        public void Test1(string basePath, string filter, int count)
//        {
//            var items = FileSystems.FindFilteredDirectories(basePath, filter);
//            Assert.Equal(items.Length, count);
//        }
//
//        [Fact]
//        public void Test2()
//        {
//            var basePath = @"Z:\b2a\base2art.standard.webapirunner";
//            var filter = "src/*/bin/Debug/*/";
//            var count = 38;
//
//            var items = FileSystems.FindFilteredDirectories(basePath, filter);
//            Assert.Equal(items.Length, count);
//            Assert.Equal(items[0], @"Z:\b2a\base2art.standard.webapirunner\src\Base2art.Proxies\bin\Debug\netstandard1.3");
//        }
//    }
//}

